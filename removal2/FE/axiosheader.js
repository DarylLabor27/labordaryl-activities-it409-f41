    const instance = axios.create({
        baseURL: 'http://127.0.0.1:8000/api/auth'
    });
    setHeaders();
    function setHeaders() {
        var accessToken = localStorage.getItem("access_token");
        instance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
    }
