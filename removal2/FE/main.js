const app = Vue.createApp({
    data() {
        return {
            showtab: "",
            userVerified: null,
            message: "",
            error: "",
            fname: "",
            lname: "",
            mname: "",
            email: "",
            password: "",
            confirm_password: ""
        }
    },
    methods: {
        showlogin() {
            this.clear()
            this.showtab = false
            this.message = ""
            this.error = ""
        },
        showregister() {
            this.clear()
            this.message = ""
            this.error = ""
            this.showtab = true
        },
        refresh() {
            instance.post('/refresh').then(({
                data
            }) => {
                console.log(data.access_token)
                localStorage.setItem("access_token", data.access_token);
                setHeaders()
                this.verify()
            }).catch((err) => {
                this.userVerified = false
                this.showtab = false
            });
        },
        login() {
            var formData = new FormData();
            formData.append('email', this.email);
            formData.append('password', this.password);
            instance.post('/login',
                formData
            ).then(({
                data
            }) => {
                localStorage.setItem("access_token", data.access_token);
                this.showtab = false;
                this.userVerified = true
                this.clear()
            }).catch((err) => {
                this.error = "Incorrect username or password."
            });
        },
        register() {
            this.error = ""
            var formData = new FormData();
            formData.append('fname', this.fname);
            formData.append('lname', this.lname);
            formData.append('mname', this.mname);
            formData.append('email', this.email);
            formData.append('password', this.password);
            formData.append('password_confirmation', this.confirm_password);
            instance.post('/register',
                formData
            ).then(({
                data
            }) => {
                this.message = "Succcessful: You are now registered!"
                this.clear()
            }).catch((err) => {
                this.message = ""
                this.error = "Failed: Please make sure that all fields are not empty. Also, please make sure that your email is a valid email and password and confirm password should match."
            });

        },
        validate(input) {
            var input = document.querySelector("#" + input);
            if (input.value == "") {
                input.classList.add('is-invalid');
                return false;
            } else {
                input.classList.remove('is-invalid');
                return true;
            }
        },
        verify() {
            console.log(localStorage.getItem("access_token"))
            instance.get('/user-profile').then(({
                data
            }) => {
                this.showtab = null;
                this.userVerified = true
            }).catch((err) => {});
        },
        logout() {
            instance.post('/logout').then(({
                data
            }) => {
                this.userVerified = false
                localStorage.removeItem("access_token")
                window.location.href = "index.html"
            }).catch((err) => {
                if (err.response.status == "422")
                    this.error = "Incorrect username or password."
            });
        },
        clear() {
            this.fname = ""
            this.lname = ""
            this.mname = ""
            this.email = ""
            this.password = ""
            this.confirm_password = ""
        }
    },
    beforeMount() {
        this.refresh()
    }
});

app.mount("#vue")
